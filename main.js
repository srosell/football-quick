new Vue({
	el: '#app',
	data: {
		kicker: {
			name: 'Kicker',
			goals: 0,
			kicks: 0,
			lives: 3,
			isAlive: true
		},
		goalkeeper: {
			name: 'Goalkeeper',
			goals: 0,
			kicks: 0,
			lives: 3,
			isAlive: true
		},
		players: [
			{
				name: 'El Semas',
				goals: 0,
				kicks: 0,
				lives: 3,
				isAlive: true
			},
			{
				name: 'Rodri',
				goals: 0,
				kicks: 0,
				lives: 3,
				isAlive: true
			},
			{
				name: 'Gato Ricardo',
				goals: 0,
				kicks: 0,
				lives: 3,
				isAlive: true
			},{
				name: 'Alejandro',
				goals: 0,
				kicks: 0,
				lives: 3,
				isAlive: true
			},{
				name: 'Gray',
				goals: 0,
				kicks: 0,
				lives: 3,
				isAlive: true
			},{
				name: 'Lo Petit',
				goals: 0,
				kicks: 0,
				lives: 3,
				isAlive: true
			}
		],
		playersAlive: [],
		playersDeath: [],
		startedGame: false
	},
	computed: {
		checkIsAlive: function () {

		}
	},
	methods: {
		createPlayers: function (player) {
			this.players.push(player);
		},
		getRandomPlayerFromArray: function (playersArray) {
			let randomInt = Math.floor(Math.random() * (playersArray.length - 0));
			let randomPlayer = playersArray[randomInt];
			playersArray.splice(randomInt, 1);
			console.log(randomInt + ' => ' + randomPlayer);
			return randomPlayer;
		},
		newGame: function () {
			this.startedGame = true;
			this.playersAlive = [];
			this.playersDeath = [];
			let playersCopy = this.players.slice();
			this.kicker = this.getRandomPlayerFromArray(playersCopy);
			console.log('Length : ' + playersCopy.length);
			this.goalkeeper = this.getRandomPlayerFromArray(playersCopy);
			console.log('Length : ' + playersCopy.length);
			while (playersCopy.length > 1) {
				this.playersAlive.push(this.getRandomPlayerFromArray(playersCopy));
				console.log('Length : ' + playersCopy.length);
			}
			this.playersAlive.push(playersCopy[0]);
		},
		goal: function () {
			this.kicker.kicks++;
			this.kicker.goals++;
			if (this.goalkeeper.lives > 0) {
				this.goalkeeper.lives--;
				if (this.goalkeeper.lives <= 0) {
					this.goalkeeperDeath();
				}else {
					this.playersAlive.push(this.kicker);
					this.kicker = this.playersAlive.length > 0 ? this.playersAlive[0] : this.kicker;
					this.playersAlive.shift();

				}
			}

		},
		fail: function () {
			this.kicker.kicks++;
			this.playersAlive.push(this.goalkeeper);
			this.goalkeeper = this.kicker;
			this.kicker = this.playersAlive.length > 0 ? this.playersAlive[0] : this.goalkeeper;
			this.playersAlive.shift();
		},
		goalkeeperDeath: function () {
			this.goalkeeper.isAlive = false;
			this.playersDeath.push(this.goalkeeper);

			if (this.playersAlive.length > 0) {
				this.goalkeeper = this.kicker;
				this.kicker = this.playersAlive[0];
				this.playersAlive.shift();
			} else {
				alert(this.kicker.name + ' Winner winner, chicken dinner!')
			}
		},
		petacuuuu: function (player) {
			if (!player.isAlive) {
				player.isAlive = true;
				this.playersDeath = this.playersDeath.filter(p => p.name !== player.name);
				this.playersAlive.push(player);
			}
			player.lives++;
		},
		minusLife: function (player) {
			if (player.lives > 0) {
				player.lives--;
			}
			if(player.lives <= 0) {
				player.isAlive = false;
				this.playersAlive = this.playersAlive.filter(p => p.name !== player.name);
				this.playersDeath.push(player);

				if (player === this.goalkeeper) {
					if (this.playersAlive.length > 0) {
						this.goalkeeper = this.kicker;
						this.kicker = this.playersAlive[0];
					} else {
						alert(this.kicker.name + ' Winner winner, chicken dinner!')
					}
				}
				if (player === this.kicker) {
					if (this.playersAlive.length > 0) {
						this.kicker = this.playersAlive[0];
						this.playersAlive.shift();
					} else {
						alert(this.goalkeeper.name + ' Winner winner, chicken dinner!')
					}
				}
			}
		}
	}

});
